package com.example.helloandroid;

public class BreedItemDto {

    private int breed;
    private int id;

    public BreedItemDto(int breed, int id) {
        this.breed = breed;
        this.id = id;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BreedItemDto{" +
                "breed=" + breed +
                ", id=" + id +
                '}';
    }
}
