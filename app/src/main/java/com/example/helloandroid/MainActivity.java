package com.example.helloandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String SHARED_PREFS_FILE = "HelloAndroid";
    private static final String TAG = "MainActivity";
    private RecyclerView rv_breed;
    private BreedItemAdapter breedItemAdapter;
    private List<BreedItemDto> breedItemDtos;
    private NestedScrollView nestedScrollView;
    private Button btn_add;
    private Button btn_reject;
    private EditText edt_breed;
    private EditText edt_id;
    private TextView txt_breed_count;
    private TextView txt_cow;
    private TextView txt_cows;
    private String BREED_KEY = "breed_key";
    private TextView btn_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        breedItemDtos = new ArrayList<>();
        initLayout();
    }

    private String htmlStringCow(){
        return "<u style='text-decoration-line: underline; text-decoration-color: red;'>COW</u>";
    }

    private String htmlStringCows(){
        return "<u style='text-decoration-line: underline; text-decoration-color: red;'>Cows: </u>";
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        editor.putString(BREED_KEY, gson.toJson(breedItemDtos));
        editor.apply();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        String json = prefs.getString(BREED_KEY, "");
        Gson gson = new Gson();
        breedItemDtos = gson.fromJson(json, new TypeToken<List<BreedItemDto>>(){}.getType());
        breedItemAdapter.setData(breedItemDtos);
        breedItemAdapter.notifyDataSetChanged();
        txt_breed_count.setText(String.valueOf(breedItemDtos.size()));
        clearBreedCount();

    }



    private void initLayout(){
        txt_cow = findViewById(R.id.txt_cow);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt_cow.setText(Html.fromHtml(htmlStringCow(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            txt_cow.setText(Html.fromHtml(htmlStringCow()));
        }

        txt_cows = findViewById(R.id.txt_cows);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt_cows.setText(Html.fromHtml(htmlStringCows(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            txt_cows.setText(Html.fromHtml(htmlStringCows()));
        }

        nestedScrollView = findViewById(R.id.nestedScrollView);
        nestedScrollView.setVerticalScrollBarEnabled(true);

        rv_breed = findViewById(R.id.rv_breed);
        rv_breed.setLayoutManager(new LinearLayoutManager(this));
        breedItemAdapter = new BreedItemAdapter(this, breedItemDtos);
        rv_breed.setAdapter(breedItemAdapter);
        ViewCompat.setNestedScrollingEnabled(rv_breed, false);

        edt_breed = findViewById(R.id.edt_breed);
        edt_id = findViewById(R.id.edt_id);

        btn_add = findViewById(R.id.btn_add);
        btn_add.setOnClickListener(v -> {
            addBreedCount();
        });

        btn_reject = findViewById(R.id.btn_reject);
        btn_reject.setOnClickListener(v-> {
            clearBreedCount();
        });

        btn_clear = findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(v-> {
            clearAll();
        });

        txt_breed_count = findViewById(R.id.txt_breed_count);
        bindEditText();

    }

    private void clearAll() {
        clearBreedCount();
        breedItemDtos.clear();
        txt_breed_count.setText("0");
        breedItemAdapter.notifyDataSetChanged();
    }

    private void clearBreedCount() {
        edt_breed.setText("");
        edt_id.setText("");
    }

    private void addBreedCount(){
        String breed = edt_breed.getText().toString().trim();
        if(TextUtils.isEmpty(breed)){
            Toast.makeText(this,"Breed is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        String id = edt_id.getText().toString().trim();
        if(TextUtils.isEmpty(id)){
            Toast.makeText(this,"ID is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        try{
            addBreedCount(Integer.parseInt(breed), Integer.parseInt(id));
        }catch (NumberFormatException e){
            Toast.makeText(this,"Breed or id is not a number", Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.getMessage());
        }

    }

    private void addBreedCount(int breed, int id) {
        BreedItemDto breedItemDto = new BreedItemDto(breed, id);
        breedItemDtos.add(0, breedItemDto);
        breedItemAdapter.notifyDataSetChanged();
        txt_breed_count.setText(String.valueOf(breedItemDtos.size()));
    }

    private void bindEditText() {
        edt_breed.addTextChangedListener(new BreedWatcher(this, this.edt_breed));
        edt_id.addTextChangedListener(new BreedWatcher(this, this.edt_id));
    }
}