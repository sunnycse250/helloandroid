package com.example.helloandroid;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class BreedItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "BreedItemAdapter";

    private List<BreedItemDto> breedItemDtos;
    private Activity mActivity;
    public static final int HEADER_ITEM = 1;
    public static final int MAIN_ITEM = 2;

    public BreedItemAdapter(Activity mActivity, List<BreedItemDto> breedItemDtos) {
        this.mActivity = mActivity;
        this.breedItemDtos = breedItemDtos;
    }

    public void setData(List<BreedItemDto> breedItemDtos){
        this.breedItemDtos = breedItemDtos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_breed, parent, false);
            return new HeaderHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_breed, parent, false);
            return new MainHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case HEADER_ITEM:
                break;
            case MAIN_ITEM:
                MainHolder mainHolder = (MainHolder) holder;
                final BreedItemDto dto = breedItemDtos.get(position - 1);
                mainHolder.txt_breed.setText(String.valueOf(dto.getBreed()));
                mainHolder.txt_id.setText(String.valueOf(dto.getId()));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_ITEM;
        } else {
            return MAIN_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return breedItemDtos == null ? 1 : breedItemDtos.size() + 1;
    }

    static class MainHolder extends RecyclerView.ViewHolder {
        TextView txt_breed;
        TextView txt_id;

        public MainHolder(View itemView) {
            super(itemView);
            txt_breed = itemView.findViewById(R.id.txt_breed);
            txt_id = itemView.findViewById(R.id.txt_id);
        }
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }

}
