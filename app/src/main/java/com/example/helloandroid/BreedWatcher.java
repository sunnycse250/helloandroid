package com.example.helloandroid;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

public class BreedWatcher implements TextWatcher {

    private EditText editText;
    private  Context context;

    BreedWatcher(Context context, EditText editText) {
        this.context = context;
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String breedText = s.toString();
        if(!breedText.toString().matches("[0-9]+"))
            Toast.makeText(context, "Please enter a number with a value between 0 and 999", Toast.LENGTH_SHORT).show();
        breedText = breedText.replaceAll("[^A-Za-z]","");
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
